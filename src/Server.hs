{-# LANGUAGE OverloadedStrings #-}

module Server where

import Servant
import Network.Wai.Handler.Warp
import Control.Concurrent.MVar
import Control.Monad.State.Strict
import Control.Monad.IO.Class
import Control.Monad.Catch (bracketOnError)
import Data.Aeson hiding ((.=))
import Data.Text hiding (count, map)
import Data.Map (toList)
import Control.Lens

import API
import Store

type M a = StateT Store Handler a

newEvent :: NewEvent -> M NewEventResponse
newEvent e = case e^?asEntry of
  Just e' -> do
    i <- use count
    count += 1
    entries.at i .= Just e'
    return $ SuccessResponse "success" "registered" i
  Nothing -> throwError $ err400 { errBody = encode err } where
    err = FailureResponse "failure" "invalid date format"

allEvents :: M Events
allEvents = do
  es <- map toEvent . toList <$> use entries
  return $ Events es

singleEvent :: Int -> M Event
singleEvent i = do
  e <- preuse $ entries.ix i
  case e of
    Just e' -> return $ toEvent (i,e')
    Nothing -> throwError $ err404 { errBody = "" }

loadState :: MVar Store -> M a -> Handler a
loadState store m = bracketOnError
  (liftIO $ takeMVar store)
  (\v -> liftIO $ putMVar store v) $ \v -> do
    (a,v') <- runStateT m v
    liftIO $ putMVar store v'
    return a

server :: MVar Store -> Server API
server store = hoistServer api (loadState store) $ newEvent :<|> allEvents :<|> singleEvent

