{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Store where

import Control.Lens
import Data.Time
import Data.Time.RFC3339
import qualified Data.Map as M
import Data.Text

import API

data Entry = Entry {
  deadline :: UTCTime,
  title :: Text,
  memo :: Text
}

data Store = Store {
  _count :: Int,
  _entries :: M.Map Int Entry
}
makeLenses ''Store

inJapan :: TimeZone
inJapan = TimeZone (9*60) False "JST"

asTime :: Prism' Text UTCTime
asTime = prism' present parse where
  present :: UTCTime -> Text
  present = formatTimeRFC3339 . utcToZonedTime inJapan
  parse :: Text -> Maybe UTCTime
  parse = fmap zonedTimeToUTC . parseTimeRFC3339

asEntry :: Prism' NewEvent Entry
asEntry = prism' present parse where
  present :: Entry -> NewEvent
  present (Entry d t m) = NewEvent (review asTime d) t m
  parse :: NewEvent -> Maybe Entry
  parse (NewEvent d t m) = Entry <$> d^?asTime ?? t ?? m

toEvent :: (Int, Entry) -> Event
toEvent (i, Entry d t m) = Event i (review asTime d) t m

initialStore :: Store
initialStore = Store 0 M.empty
