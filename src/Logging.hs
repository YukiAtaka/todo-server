{-# LANGUAGE OverloadedStrings #-}

module Logging where

import Network.Wai
import qualified Data.ByteString as B
import Data.Monoid

logging :: Middleware
logging app req resp = do
  B.putStr $ requestMethod req <> " " <> rawPathInfo req
  putStrLn ""
  app req resp
