{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeOperators #-}

module API (
  NewEvent(NewEvent),
  NewEventResponse(SuccessResponse, FailureResponse),
  Event(Event),
  Events(Events),
  APIv1, API, api
) where

import Servant
import GHC.Generics
import Data.Aeson
import Data.Text

data NewEvent = NewEvent {
  deadline :: Text,
  title :: Text,
  memo :: Text
} deriving (Eq, Show, Generic, FromJSON, ToJSON)

data NewEventResponse =
  SuccessResponse {
    status :: Text,
    message :: Text,
    id :: Int
  } | FailureResponse {
    status :: Text,
    message :: Text
  } deriving (Show, Generic, ToJSON)

data Event = Event {
  id :: Int,
  deadline :: Text,
  title :: Text,
  memo :: Text
} deriving (Show, Generic, ToJSON)

data Events = Events {
  events :: [Event]
} deriving (Show, Generic, ToJSON)

type APIv1 = "event" :> ReqBody '[JSON] NewEvent :> Post '[JSON] NewEventResponse
        :<|> "event" :> Get '[JSON] Events
        :<|> "event" :> Capture "id" Int :> Get '[JSON] Event
type API = "api" :> "v1" :> APIv1

api :: Proxy API
api = Proxy

