module Main where

import Network.Wai.Handler.Warp
import Servant
import GHC.Generics
import Data.Aeson
import Control.Concurrent.MVar

import API
import Server
import Store (initialStore)
import Logging

main :: IO ()
main = do
  store <- newMVar initialStore
  let port = 8080
  putStrLn $ "Launched on port " ++ show port
  run port $ logging $ serve api (server store)
