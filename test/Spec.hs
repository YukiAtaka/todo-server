{-# LANGUAGE OverloadedStrings #-}

module Main where

import Test.Hspec
import Test.Hspec.Wai
import Test.Hspec.Wai.Matcher
import Network.Wai.Test hiding (request)
import Network.HTTP.Types
import Test.QuickCheck.Property
import Test.QuickCheck.Instances.Time
import Servant
import Control.Concurrent.MVar
import Control.Lens
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as LB
import Data.Monoid
import Data.Aeson
import Data.Text.Encoding
import Data.Text.Arbitrary
import Data.Time.Clock
import Data.String

import API
import Store
import Server

main :: IO ()
main = hspec spec

launcher :: IO Application
launcher = do
  store <- newMVar initialStore
  return $ serve api (server store)

losePrecision :: UTCTime -> UTCTime
losePrecision (UTCTime x d) = UTCTime x d' where
  d' = secondsToDiffTime $ diffTimeToPicoseconds d `div` 10^12

postJSON :: B.ByteString -> LB.ByteString -> WaiSession SResponse
postJSON path = request methodPost path [("Content-Type","application/json")]

spec :: Spec
spec = with launcher $ do
  describe "json decoding" $ do
    it "converts correct JSON to UTCTime" $ \_ -> property $ \d' -> let
        d = losePrecision d'
        json = show $ review asTime d
        json' = fromString json
      in decode json' === Just d
    it "converts correct JSON to NewEvent" $ \_ -> property $ \d t m -> let
        td = review asTime d
        json = "{\"deadline\":" <> encode td <> ",\"title\":" <> encode t <> ",\"memo\":" <> encode m <> "}"
        e = NewEvent td t m
      in decode json === Just e .||. ":" <> json === encode e
  describe "all events API" $ do
    it "returns an empty list at first" $ do
      get "/api/v1/event" `shouldRespondWith` "{\"events\":[]}"
    it "returns a singleton list after inserting an element" $ do
      let ne = NewEvent "2019-06-11T14:00:00+09:00" "Title" "Memo"
      let e = Event 0 "2019-06-11T14:00:00+09:00" "Title" "Memo"
      let res = bodyEquals $ "{\"events\":[" <> encode e <> "]}"
      (postJSON "/api/v1/event" (encode ne) >> get "/api/v1/event") `shouldRespondWith` ResponseMatcher 200 [] res
  describe "add event API" $ do
    it "responds with 200 to correct event" $ do
      let ne = NewEvent "2019-06-03T10:00:00+09:00" "Title" "Memo"
      postJSON "/api/v1/event" (encode ne) `shouldRespondWith` 200
    it "responds with 400 to malformed time" $ do
      let ne = NewEvent "2019-06-03 10:00:00" "Title" "Memo"
      postJSON "/api/v1/event" (encode ne) `shouldRespondWith` 400
  describe "single event API" $ do
    it "responds with 404 at the first time" $ do
      get "/api/v1/event/3" `shouldRespondWith` 404
    it "returns event succesfully after inserting an element" $ do
      let ne = NewEvent "2019-06-01T19:00:00+09:00" "Title" "Memo"
      let e = Event 0 "2019-06-01T19:00:00+09:00" "Title" "Memo"
      let res = bodyEquals $ encode e
      (postJSON "/api/v1/event" (encode ne) >> get "/api/v1/event/0") `shouldRespondWith` ResponseMatcher 200 [] res
  describe "sequential API execution" $ do
    it "behaves as expected" $ do
      get "/api/v1/event/3" `shouldRespondWith` 404
      let ne1 = NewEvent "2019-06-06T00:00:00+09:00" "A" "B"
      postJSON "/api/v1/event" (encode ne1) `shouldRespondWith` 200
      let ne2 = NewEvent "2019/06/07" "C" "D"
      postJSON "/api/v1/event" (encode ne2) `shouldRespondWith` 400
      let ne3 = NewEvent "2019-06-08T17:00:00+09:00" "E" "F"
      let resSuc = bodyEquals $ encode $ SuccessResponse "success" "registered" 1
      postJSON "/api/v1/event" (encode ne3) `shouldRespondWith` ResponseMatcher 200 [] resSuc
      let resEv = bodyEquals $ encode $ Events [Event 0 "2019-06-06T00:00:00+09:00" "A" "B", Event 1 "2019-06-08T17:00:00+09:00" "E" "F"]
      get "/api/v1/event" `shouldRespondWith` ResponseMatcher 200 [] resEv
