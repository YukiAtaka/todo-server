# todo-server

## 概要

- 言語: Haskell (Stack)
    - ビルド: `stack build`
    - 実行 : `stack run`
    - テスト: `stack test`
- 起動すると8080番をlistenします
    - 全イベント取得API `GET /api/v1/event`
    - イベント登録API `POST /api/v1/event`
        - Content-Type指定が必須
    - 指定イベント取得API `GET /api/v1/event/{id}`

## 構成

### 使用基盤

- 基盤系ライブラリ
    - servant (ルーティング・データ変換)
    - wai (WAI)
    - warp (Webサーバ)
- テスト系ライブラリ
    - HSpec (テストフレームワーク)
    - QuickCheck (自動テストケース生成)
- 補助系ライブラリ
    - lens (データアクセス)
    - aeson (JSON)
    - timerep (RFC3339に則った時間変換)

### ソース構造

- app/
    - Main.hs: プログラムエントリポイント
- src/
    - API.hs: API定義
    - Server.hs: サーバ本体実装
    - Logging.hs: 簡易ロガー実装
    - Store.hs: 保持データ定義
- test/
    - Spec.hs: テスト
